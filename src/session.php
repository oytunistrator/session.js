<?php
namespace SessionJS;
class Session
{
    function __construct($id){
        session_id($id);
        session_start();
        self::session = $_SESSION;
    }

    public function __key($key = null){
        if(!is_null($key)){
            return $this->session[$key];
        }
        return false;
    }

    public function toJson($key){
        header('Content-Type: application/json');
        $data['result'] = $this->__key($key);
        echo json_encode($data);
    }
}